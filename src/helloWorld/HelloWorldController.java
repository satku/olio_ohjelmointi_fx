package helloWorld;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

public class HelloWorldController {
	@FXML
	private Button button;
	@FXML
	private Label label;
	@FXML
	private Button clearButton;
	@FXML
	private TextField inputField;
	@FXML
	private Slider sizeSlider;

	// Event Listener on Button[#button].onAction
	@FXML
	public void handleButtonAction(ActionEvent event) {
		System.out.println("Text added");
		label.setText(label.getText() + inputField.getText());
		inputField.clear();
	}

	// Event Listener on Button[#clearButton].onAction
	@FXML
	public void clearAction(ActionEvent event) {
		label.setText("");
	}

	// Event Listener on Slider[#sizeSlider].onMouseDragged
	@FXML
	public void sliderChangeAction(MouseEvent event) {
		label.setFont(new Font(sizeSlider.getValue()));
	}
}
