package webBrowser;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.web.WebView;
import javafx.scene.control.TextField;
import java.util.ArrayList;
import java.util.ListIterator;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;

public class WebBrowserController {
	@FXML
	private TextField urlBox;
	@FXML
	private Button loadButton;
	@FXML
	private Button refreshButton;
	@FXML
	private WebView web;
	@FXML
	private Button shoutOutButton;
	@FXML
	private Button initializeButton;
	@FXML
	private Button previousButton;
	@FXML
	private Button nextButton;

	ArrayList<String> history = null;
	int historyLocation = 0;

	//cmd+shift+l for help
	//+cmd+alt+r for rename

    public void initialize() {
    		history = new ArrayList<String>();
		web.getEngine().load("https://www.lut.fi/");
		history.add(web.getEngine().getLocation());
	}

	// Event Listener on Button[#loadButton].onAction
	@FXML
	public void loadPage(ActionEvent event) {
		if (urlBox.getText().equals("index.html")) {
			web.getEngine().load(getClass().getResource("index.html").toExternalForm());
		} else {
			if (urlBox.getText().length() > 12) {
				if (urlBox.getText().substring(0, 12).equals("https://www.") || urlBox.getText().substring(0, 11).equals("http://www.") || urlBox.getText().substring(0, 8).equals("https://") || urlBox.getText().substring(0, 10).equals("http://")) {
					System.out.println("tööööööt\n");
					web.getEngine().load(urlBox.getText());
				} else {
					web.getEngine().load("https://" + urlBox.getText());
				}
			} else {
				web.getEngine().load("https://" + urlBox.getText());
			}
		}
		if (web.getEngine().getLocation().length() > 12) {
			if (!history.get(history.size() - 1).equals(web.getEngine().getLocation()) && !history.get(history.size() - 1).equals("https://" + web.getEngine().getLocation().substring(12)) && !history.get(history.size() - 1).equals("http://" + web.getEngine().getLocation().substring(12, web.getEngine().getLocation().length() - 1))) {
				if (historyLocation != 0) {
					String lastHistory = history.get(history.size() - 1 - historyLocation);
		    			history = new ArrayList<String>();
		    			history.add(lastHistory);
					history.add(web.getEngine().getLocation());
					historyLocation = 0;
				} else {
					history.add(web.getEngine().getLocation());
				}
			}
		} else {
			if (!history.get(history.size() - 1).equals(web.getEngine().getLocation())) {
				if (historyLocation != 0) {
					String lastHistory = history.get(history.size() - 1 - historyLocation);
	    				history = new ArrayList<String>();
	    				history.add(lastHistory);
	    				history.add(web.getEngine().getLocation());
					historyLocation = 0;
				} else {
					history.add(web.getEngine().getLocation());
				}
			}
		}
		urlBox.setText(web.getEngine().getLocation());
	}

	// Event Listener on Button[#refreshButton].onAction
	@FXML
	public void refreshPage(ActionEvent event) {
		web.getEngine().load(web.getEngine().getLocation());
	}

	// Event Listener on WebView[#web].onMouseClicked
	@FXML
	public void getUrl(MouseEvent event) {
		if (web.getEngine().getLocation().length() > 12) {
			if (!history.get(history.size() - 1).equals(web.getEngine().getLocation()) && !history.get(history.size() - 1).equals("https://" + web.getEngine().getLocation().substring(12)) && !history.get(history.size() - 1).equals("http://" + web.getEngine().getLocation().substring(12, web.getEngine().getLocation().length() - 1))) {
				if (historyLocation != 0) {
					String lastHistory = history.get(history.size() - 1 - historyLocation);
	    				history = new ArrayList<String>();
	    				history.add(lastHistory);
	    				history.add(web.getEngine().getLocation());
					historyLocation = 0;
				} else {
					history.add(web.getEngine().getLocation());
				}
			}
		} else {
			if (!history.get(history.size() - 1).equals(web.getEngine().getLocation())) {
				if (historyLocation != 0) {
					String lastHistory = history.get(history.size() - 1 - historyLocation);
	    				history = new ArrayList<String>();
	    				history.add(lastHistory);
	    				history.add(web.getEngine().getLocation());
					historyLocation = 0;
				} else {
					history.add(web.getEngine().getLocation());
				}
			}
		}
		urlBox.setText(web.getEngine().getLocation());

		//testiajoa
		/*for (int bottleIndex = 0; history.size() > bottleIndex; bottleIndex++) {
			System.out.println("historia: " + history.get(bottleIndex));
		}
		System.out.println();*/

	}

	// Event Listener on Button[#shoutOutButton].onAction
	@FXML
	public void shoutOut(ActionEvent event) {
		web.getEngine().executeScript("document.shoutOut()");
	}

	// Event Listener on Button[#initializeButton].onAction
	@FXML
	public void initialize(ActionEvent event) {
		web.getEngine().executeScript("initialize()");
	}

	// Event Listener on Button[#previousButton].onAction
	@FXML
	public void loadPrevious(ActionEvent event) {
		ListIterator<String> litr = history.listIterator(history.size() - 1 - historyLocation);
		if (litr.hasPrevious()) {
			web.getEngine().load(litr.previous());
			historyLocation++;
		}
		urlBox.setText(web.getEngine().getLocation());
	}

	// Event Listener on Button[#nextButton].onAction
	@FXML
	public void loadNext(ActionEvent event) {
		ListIterator<String> litr = history.listIterator(history.size() - historyLocation);
		if (litr.hasNext()) {
			web.getEngine().load(litr.next());
			historyLocation--;
		}
		urlBox.setText(web.getEngine().getLocation());
	}
}