/**
 * @author elu - Esko Heino
 * Bottle.java
 * Date: 1 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package bottleDispenser;

/**
 *
 */
public class Bottle {
	private String name;
	private double size;
	private double price;

	public Bottle(String bottleName, double bottleSize, double bottlePrice) {
		name = bottleName;
		size = bottleSize;
		price = bottlePrice;
	}

	public String getName() {
		return name;
	}

	public double getSize() {
		return size;
	}

	public double getPrice() {
		return price;
	}
}