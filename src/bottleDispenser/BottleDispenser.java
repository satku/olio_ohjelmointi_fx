/**
 * @author elu - Esko Heino
 * BottleDispenser.java
 * Date: 1 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package bottleDispenser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 */
public class BottleDispenser {

	private int bottles;
	private float money;
	private ArrayList<Bottle> bottle_arrayList = new ArrayList<Bottle>();
	static private BottleDispenser instance = null;

	private BottleDispenser() {
		bottles = 6;
		money = 0;
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Pepsi Max", 0.5, 1.8));
		}
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Pepsi Max", 1.5, 2.2));
		}
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Coca-Cola Zero", 0.5, 2.0));
		}
		for (int bottleIndex = 0; bottleIndex < 1; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Coca-Cola Zero", 1.5, 2.5));
		}
		for (int bottleIndex = 0; bottleIndex < 2; bottleIndex++) {
			bottle_arrayList.add(new Bottle("Fanta Zero", 0.5, 1.95));
		}
	}

	static public BottleDispenser getInstance() {
		if (instance == null) {
			instance = new BottleDispenser();
		}
		return instance;
	}

	public void addMoney(double amount) {
		money += amount;
	}

	public String buyBottle(String name, Double number) throws IOException {
		if (bottles == 0) {
			return "Kaikki pullot ovat loppu.";
		}

		for (int bottleIndex = 0; bottle_arrayList.size() > bottleIndex; bottleIndex++) {
			if (bottle_arrayList.get(bottleIndex).getName() == name && bottle_arrayList.get(bottleIndex).getSize() == number) {
				if (bottle_arrayList.get(bottleIndex).getPrice() > money) {
					if (money > 0) {
						return "Pullo maksaa " + bottle_arrayList.get(bottleIndex).getPrice() + " € ja sinulla on rahaa " + money + " €.";
					} else {
						return "Sinulla ei ole yhtään rahaa.";
					}
				} else {
					Double price = bottle_arrayList.get(bottleIndex).getPrice();
					bottles--;
					money -= bottle_arrayList.get(bottleIndex).getPrice();
					bottle_arrayList.remove(bottleIndex);

					BufferedWriter out = new BufferedWriter(new FileWriter("receipt.txt"));
					out.write("\tKuitti\n\n" + name + " " + number + "l\t" + price + " €");
					out.close();

					if (money > 0) {
						return "Ostit " + number + "l " + name + " pullon hintaan " + price + " €.\nSinulla on vielä jäljellä " + money + " €.";
					} else {
						return "Ostit " + number + "l " + name + " pullon hintaan " + price + " €.\nSinulla ei ole enää rahaa jäljellä.";
					}
				}
			}
		}

		return "Kaikki " + number + "l " + name + " pullot ovat loppu.";
	}

	public float returnMoney() {
		float cashAmount = money;
		money = 0;
		return cashAmount;
	}

}