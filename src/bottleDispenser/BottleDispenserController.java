package bottleDispenser;

import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javafx.event.ActionEvent;

import javafx.scene.control.Slider;

import javafx.scene.control.TextArea;

import javafx.scene.control.ComboBox;

import javafx.scene.input.MouseEvent;

public class BottleDispenserController {
	@FXML
	private Button addMoneyButton;
	@FXML
	private Button cashButton;
	@FXML
	private TextArea textArea;
	@FXML
	private Button buyButton;
	@FXML
	private Slider moneySlider;
	@FXML
	private ComboBox<String> bottles;
	@FXML
	private ComboBox<Double> bottleSizes;
	@FXML
	private TextField moneyAmount;

	BottleDispenser user1 = null;
	double amount = 0;

	@FXML
    public void initialize() {
		user1 = BottleDispenser.getInstance();

		textArea.setWrapText(true);
		textArea.setText("LIMSA-AUTOMAATTI\n\nLisää ensin rahaa koneeseen ja sen jälkeen valitse tuote sekä koko pudotusvalikoista.");

		bottles.getItems().add("Pepsi Max");
		bottles.getItems().add("Coca-Cola Zero");
		bottles.getItems().add("Fanta Zero");
		bottleSizes.getItems().add(0.5);
		bottleSizes.getItems().add(1.0);
		bottleSizes.getItems().add(1.5);

    }

	// Event Listener on Button[#addMoneyButton].onAction
	@FXML
	public void addMoney(ActionEvent event) {
		user1.addMoney(amount);
		textArea.setText("Lisäsit rahaa " + amount + " €.");
		moneyAmount.setText("");
	}

	// Event Listener on Button[#cashButton].onAction
	@FXML
	public void cashMoney(ActionEvent event) {
		float returnAmount = user1.returnMoney();
		if (returnAmount > 0) {
			textArea.setText("Sait rahaa " + returnAmount + " €.");
		} else {
			textArea.setText("Et saanut yhtään rahaa.");
		}
	}

	// Event Listener on Button[#buyButton].onAction
	@FXML
	public void buyBottle(ActionEvent event) throws IOException {
		String bottleName = bottles.valueProperty().getValue();
		Double bottleSize = bottleSizes.valueProperty().getValue();

		if (bottleName != null && bottleSize != null) {
			textArea.setText(user1.buyBottle(bottleName, bottleSize));
		} else {
			textArea.setText("Valitse ensin pudotusvalikoista haluamasi pullo sekä koko.");
		}
	}

	// Event Listener on Slider[#moneySlider].onMouseDragged
	@FXML
	public void sliderChangeAction(MouseEvent event) {
		DecimalFormat df = new DecimalFormat("#.#");
		df.setRoundingMode(RoundingMode.CEILING);

		Double d = moneySlider.getValue();
		String s = df.format(d);
		moneyAmount.setText(s);
		amount = Double.parseDouble(s);
	}

}
