/**
 * @author elu - Esko Heino
 * GetShows.java
 * Date: 16 Nov 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package finnkino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 */
public class GetShows {
	static private GetShows instance = null;
	private ObservableList<String> shows_observableList = null;
	private Document doc;
	private String name = "";

	private GetShows() {
	}

	static public GetShows getInstance() throws IOException {
		if (instance == null) {
			instance = new GetShows();
		}
		return instance;
	}

	public void loadShows(String n) throws IOException {
		name = n;
		shows_observableList = FXCollections.observableArrayList();
		GetTheaters loadTheaters = GetTheaters.getInstance();
		ArrayList<Theater> theater_arrayList = loadTheaters.getTheaters();

		if (n.trim().isEmpty()) {
			shows_observableList.add("Anna ensin elokuvan nimi");
			return;
		} else {
			shows_observableList.add("\t\t\t" + name);
		}

		for (int movieIndex = 1; theater_arrayList.size() > movieIndex; movieIndex++) {
			String line;
			String content = "";
			int place = 0;

			place = theater_arrayList.get(movieIndex).getId();

			URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + place + "&dt=");

			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

			while ((line = br.readLine()) != null) {
				content += line;
			}

			parseXml(content);
		}
	}

	private void parseXml(String xmlFile) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(new InputSource(new StringReader(xmlFile)));

			doc.getDocumentElement().normalize();

			parseCurrentData();

		} catch (ParserConfigurationException | SAXException | IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseCurrentData() throws ParseException {
		NodeList nodes = doc.getElementsByTagName("Show");

		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;

			if (getValue("Title", e).equals(name)) {
				shows_observableList.add(getValue("Theatre", e) + " \t" + getValue("dttmShowStart", e));
			}

		}
	}

	private String getValue(String tag, Element e) {
		return e.getElementsByTagName(tag).item(0).getTextContent();
	}

	public ObservableList<String> getShows() {
		return shows_observableList;
	}

}
