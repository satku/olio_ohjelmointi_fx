/**
 * @author elu - Esko Heino
 * Theater.java
 * Date: 15 Nov 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package finnkino;

/**
 *
 */
public class Theater {
	private int id;
	private String place;

	public Theater(int i, String p) {
		id = i;
		place = p;
	}

	public String getPlace() {
		return place;
	}

	public int getId() {
		return id;
	}

}
