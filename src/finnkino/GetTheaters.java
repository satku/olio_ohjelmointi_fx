/**
 * @author elu - Esko Heino
 * GetTheaters.java
 * Date: 15 Nov 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package finnkino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 */
public class GetTheaters {
	static private GetTheaters instance = null;
	private ArrayList<Theater> theater_arrayList = new ArrayList<Theater>();
	private Document doc;

	private GetTheaters() throws IOException {
		String line;
		String content = "";

		URL url = new URL("http://www.finnkino.fi/xml/TheatreAreas/");

		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

		while ((line = br.readLine()) != null) {
			content += line;
		}

		parseXml(content);

	}

	static public GetTheaters getInstance() throws IOException {
		if (instance == null) {
			instance = new GetTheaters();
		}
		return instance;
	}

	private void parseXml(String xmlFile) {

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(new InputSource(new StringReader(xmlFile)));

			doc.getDocumentElement().normalize();

			parseCurrentData();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseCurrentData() {
		NodeList nodes = doc.getElementsByTagName("TheatreArea");

		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;

			theater_arrayList.add(new Theater(Integer.parseInt(getValue("ID", e)), getValue("Name", e)));
		}
	}

	private String getValue(String tag, Element e) {
		return e.getElementsByTagName(tag).item(0).getTextContent();
	}

	public ArrayList<Theater> getTheaters() {
		return theater_arrayList;
	}

}
