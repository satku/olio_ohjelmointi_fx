/**
 * @author elu - Esko Heino
 * GetMovies.java
 * Date: 16 Nov 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package finnkino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 */
public class GetMovies {
	static private GetMovies instance = null;
	private ObservableList<String> movies_observableList = null;
	private Document doc;
	private String begin = "";
	private String end = "";

	private GetMovies() {
	}

	static public GetMovies getInstance() throws IOException {
		if (instance == null) {
			instance = new GetMovies();
		}
		return instance;
	}

	public void loadMovies(String theater, String date, String b, String e) throws IOException {
		begin = b;
		end = e;
		movies_observableList = FXCollections.observableArrayList();
		GetTheaters loadTheaters = GetTheaters.getInstance();
		ArrayList<Theater> theater_arrayList = loadTheaters.getTheaters();

		String line;
		String content = "";
		int place = 0;

		for (int movieIndex = 1; theater_arrayList.size() > movieIndex; movieIndex++) {
			if (theater_arrayList.get(movieIndex).getPlace() == theater) {
				place = theater_arrayList.get(movieIndex).getId();
			}
		}

		URL url = new URL("http://www.finnkino.fi/xml/Schedule/?area=" + place + "&dt=" + date);

		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));

		while ((line = br.readLine()) != null) {
			content += line;
		}

		parseXml(content);
	}

	private void parseXml(String xmlFile) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

			doc = dBuilder.parse(new InputSource(new StringReader(xmlFile)));

			doc.getDocumentElement().normalize();

			parseCurrentData();

		} catch (ParserConfigurationException | SAXException | IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void parseCurrentData() throws ParseException {
		NodeList nodes = doc.getElementsByTagName("Show");

		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;

			Date date = new Date() ;
			SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm") ;
			dateFormat.format(date);

			if (begin.trim().isEmpty() && end.trim().isEmpty()) {
				movies_observableList.add(getValue("Title", e) + " \t" + getValue("dttmShowStart", e));

			} else if (!begin.trim().isEmpty() && end.trim().isEmpty()) {
				if (dateFormat.parse(getValue("dttmShowStart", e).substring(11, 16)).after(dateFormat.parse(begin))) {
					movies_observableList.add(getValue("Title", e) + " \t" + getValue("dttmShowStart", e));
				}

			} else if (begin.trim().isEmpty() && !end.trim().isEmpty()) {
				if (dateFormat.parse(end).after(dateFormat.parse(getValue("dttmShowStart", e).substring(11, 16)))) {
					movies_observableList.add(getValue("Title", e) + " \t" + getValue("dttmShowStart", e));
				}

			} else if (!begin.trim().isEmpty() && !end.trim().isEmpty()) {
				if (dateFormat.parse(getValue("dttmShowStart", e).substring(11, 16)).after(dateFormat.parse(begin)) && dateFormat.parse(end).after(dateFormat.parse(getValue("dttmShowStart", e).substring(11, 16)))) {
						movies_observableList.add(getValue("Title", e) + " \t" + getValue("dttmShowStart", e));
				}
			}

		}
	}

	private String getValue(String tag, Element e) {
		return e.getElementsByTagName(tag).item(0).getTextContent();
	}

	public ObservableList<String> getMovies() {
		return movies_observableList;
	}

}
