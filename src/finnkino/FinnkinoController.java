package finnkino;

import javafx.fxml.FXML;

import javafx.scene.control.Button;

import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.ArrayList;

import javafx.event.ActionEvent;

import javafx.scene.control.ListView;

import javafx.scene.control.ComboBox;

public class FinnkinoController {
	@FXML
	private ComboBox<String> theaterBox;
	@FXML
	private TextField movieBegin;
	@FXML
	private Button showMoviesButton;
	@FXML
	private TextField movieDate;
	@FXML
	private TextField movieEnd;
	@FXML
	private TextField movieName;
	@FXML
	private Button movieSearchButton;
	@FXML
	private ListView<String> movies;

	@FXML
    public void initialize() throws IOException {
		GetTheaters loadTheaters = GetTheaters.getInstance();
		ArrayList<Theater> theaterList = loadTheaters.getTheaters();

		for (int theaterIndex = 1; theaterList.size() > theaterIndex; theaterIndex++) {
			theaterBox.getItems().add(theaterList.get(theaterIndex).getPlace());
		}
	}

	// Event Listener on Button[#showMoviesButton].onAction
	@FXML
	public void showMovies(ActionEvent event) throws IOException {
		String theater = theaterBox.valueProperty().getValue();
		String date = movieDate.getText();
		String begin = movieBegin.getText();
		String end = movieEnd.getText();

		GetMovies loadMovies = GetMovies.getInstance();
		loadMovies.loadMovies(theater, date, begin, end);
		movies.setItems(loadMovies.getMovies());
	}

	// Event Listener on Button[#movieSearchButton].onAction
	@FXML
	public void searchMovie(ActionEvent event) throws IOException {
		String movie = movieName.getText();

		GetShows loadShows = GetShows.getInstance();
		loadShows.loadShows(movie);
		movies.setItems(loadShows.getShows());
	}
}
