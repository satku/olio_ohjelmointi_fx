/**
 * @author elu - Esko Heino
 * ReadAndWriteIO.java
 * Date: 24 Oct 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package textEditor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 */
public class ReadAndWriteIO {

	public ReadAndWriteIO() {
	}

	public String readFile(String fileName) throws IOException {
		String file = "";
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		for (String line; (line = in.readLine()) != null;) {
			file += line + "\n";
		}
		in.close();
		return file;
	}

	public void writeFile(String saveFile, String line) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(saveFile));
		out.write(line);
		out.close();
	}

}
