package textEditor;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class TextEditorController {
	@FXML
	private Button loadButton;
	@FXML
	private Button saveButton;
	@FXML
	private TextArea textArea;
	@FXML
	private TextField inputField;

	// Event Listener on Button[#loadButton].onAction
	@FXML
	public void loadFile(ActionEvent event) throws IOException {
		ReadAndWriteIO fileReading = new ReadAndWriteIO();
		if (!inputField.getText().trim().isEmpty()) {
			String fileText = fileReading.readFile(inputField.getText());
			textArea.setText(fileText);
			inputField.setText("");
		} else {
			inputField.setText("Anna tiedostonimi!");
		}
	}

	// Event Listener on Button[#saveButton].onAction
	@FXML
	public void saveFile(ActionEvent event) throws IOException {
		ReadAndWriteIO fileWriting = new ReadAndWriteIO();
		if (textArea.getText().trim().isEmpty()) {
			textArea.setText("Haluatko varmasti\ntallentaa tyhjän\ntiedoston?");
		} else if (!inputField.getText().trim().isEmpty()) {
			fileWriting.writeFile(inputField.getText(), textArea.getText());
			textArea.setText("Tiedosto tallennettu\nonnistuneesti!");
		} else {
			inputField.setText("Anna tiedostonimi!");
		}
	}

}
