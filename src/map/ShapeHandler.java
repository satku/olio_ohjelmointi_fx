/**
 * @author elu - Esko Heino
 * ShapeHandler.java
 * Date: 27 Nov 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package map;

import java.util.ArrayList;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 */
public class ShapeHandler {
	static private ShapeHandler instance = null;
	private ArrayList<Circle> circle_arrayList = new ArrayList<Circle>();
	private ArrayList<Line> line_arrayList = new ArrayList<Line>();

	private ShapeHandler() {
	}

	static public ShapeHandler getInstance() {
		if (instance == null) {
			instance = new ShapeHandler();
		}
		return instance;
	}

	public void newShape(Circle circle) {
		circle_arrayList.add(circle);
	}

	public void newShapeLine(Line line) {
		line_arrayList.add(line);
	}

}
