package map;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;

public class MapController {
	@FXML
	private AnchorPane root;
	private int lineStart = 1;
	private int lineEnd = 2;

	// Event Listener on BorderPane.onMouseClicked
	@FXML
	public void mouseClick(MouseEvent event) {
		Point pointMaker = new Point();
        root.getChildren().add(pointMaker.newCircle(event.getX(), event.getY()));

        	root.getChildren().get(1).onMouseClickedProperty().set(new EventHandler<MouseEvent>() {
        		@Override
        		public void handle(MouseEvent e) {
        			System.out.println("Hei, olen piste!");
        		}
        	});

        	if (root.getChildren().size() > 2) {
        		for (int lineIndex = 3; root.getChildren().size() > lineIndex; lineIndex++) {
        			lineStart = lineIndex - 2;
        			lineEnd = lineIndex;
        		}
	        	Line line = new Line();

	        	line.setStartX(root.getChildren().get(lineStart).getBoundsInLocal().getMaxX() - 1.5);
	        	line.setStartY(root.getChildren().get(lineStart).getBoundsInLocal().getMaxY() - 1.5);
	        	line.setEndX(root.getChildren().get(lineEnd).getBoundsInLocal().getMaxX() - 1.5);
	        	line.setEndY(root.getChildren().get(lineEnd).getBoundsInLocal().getMaxY() - 1.5);

	        	pointMaker.newLine(line);
	        	root.getChildren().add(line);
        	}

	}
	// Event Listener on BorderPane.onScroll
	@FXML
	public void clearButton(ScrollEvent event) {
		lineStart = 1;
		lineEnd = 2;
		root.getChildren().remove(1, root.getChildren().size());
	}

}
