/**
 * @author elu - Esko Heino
 * Point.java
 * Date: 27 Nov 2017
 * Environment: macOS 10.12.6, Eclipse 4.7.0
 * Ohjelman nimi ja mitä se tekee
 */
package map;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 */
public class Point {
	private ShapeHandler storage = null;

	public Point() {
	}

	public Circle newCircle(double x, double y) {
		Circle circle = new Circle(x, y, 3, Color.BLACK);
		storage = ShapeHandler.getInstance();
		storage.newShape(circle);
		return circle;
	}

	public void newLine(Line line) {
		storage = ShapeHandler.getInstance();
		storage.newShapeLine(line);
	}

}
